from transformers import AutoModelForCausalLM, AutoTokenizer, TrainingArguments, Trainer, DataCollatorForLanguageModeling, BitsAndBytesConfig
from transformers import AutoModelForCausalLM
import torch
from datasets import load_dataset, Dataset
import datasets
from transformers import AutoModelForCausalLM, AutoTokenizer, TrainingArguments, Trainer, DataCollatorForLanguageModeling, BitsAndBytesConfig
from peft import LoraConfig, get_peft_model




model = AutoModelForCausalLM.from_pretrained("microsoft/phi-2", torch_dtype="auto", trust_remote_code=True)
tokenizer = AutoTokenizer.from_pretrained("microsoft/phi-2", trust_remote_code=True)


from datasets import load_dataset

tokenizer.pad_token = tokenizer.eos_token


def tokenize(sample):
    tokenized_text =  tokenizer(sample["text"], padding=True, truncation=True, max_length=512)
    return tokenized_text

data = load_dataset("BI55/MedText", split="train")
data_df = data.to_pandas()
data_df["text"] = data_df[["Prompt", "Completion"]].apply(lambda x: "Prompt: " + x["Prompt"] + " Completion: " + x["Completion"], axis=1)
data = Dataset.from_pandas(data_df)

tokenized_data = data.map(tokenize, batched=True, desc="Tokenizing data", remove_columns=data.column_names)



lora_config = LoraConfig(
    r=16,
    lora_alpha=16,
    target_modules=["Wqkv", "out_proj"],
    lora_dropout=0.05,
    bias="none",
    task_type="CAUSAL_LM"
)

model = get_peft_model(model, lora_config)



training_arguments = TrainingArguments(
        output_dir="phi-1_5-finetuned-med-text",
        per_device_train_batch_size=4,
        gradient_accumulation_steps=1,
        learning_rate=2e-4,
        lr_scheduler_type="cosine",
        save_strategy="epoch",
        logging_steps=100,
        max_steps=100,
        num_train_epochs=1
    )



trainer = Trainer(
    model=model,
    train_dataset=tokenized_data,
    args=training_arguments,
    data_collator=DataCollatorForLanguageModeling(tokenizer, mlm=False)
)
trainer.train()
